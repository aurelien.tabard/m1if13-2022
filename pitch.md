# La Guilde des Trouvailles

Dans un passé plus ou moins lointain, d'anciens grands sages ont caché sur Terre des coffres au trésor. Ces coffres, de formes et couleurs similaires, ont été rendus invisibles pour éviter que des puissances cupides ne s'en emparent, car ils recèlent de nombreux trésors de toutes sortes...

<img width="100%" height="200px" src="./Foret-coffre.png" alt="Forêt enchantée">

De nos jours, les sociétés secrètes de chasseurs de trésor se multiplient et ratissent le moindre bâtiment, le moindre site de fouilles. La plus aguerrie de ces sociétés, la Guilde des Trouvailles (GT), a eu vent d'une quête prodigieuse et pourtant dangereuse qui se passe dans une forêt mystérieuse gardée par un dragon. La rumeur circule que des coffres d'un ancien temps contenant des pierres précieuses et magiques réapparaitraient dans la forêt avec une régularité étonnante et attendraient juste d'être trouvés par des âmes valeureuses. 
 
Pour organiser les recherches, la GT a organisé la forêt en Zones de Recherche de Reliques (ZRR) afin que chacun de ses membres, les aventuriers, ait sa propre zone de recherche. Mais le dragon rôde… La GT fournit donc une potion de lune à chaque aventurier : une protection temporaire pour qu'ils puissent se balader comme bon leur semble dans la zone sans être repérés par le dragon. Cette potion n'est valable qu'un temps limité appelé « Temps de Tranquillité et de Liberté » (TTL). Passé ce temps, le dragon frappera tout aventurier qui n'a plus de protection, et celui-ci cessera définitivement de participer à la quête. Par chance, certains coffres renferment des potions de lune, permettant ainsi à un aventurier de recharger son TTL. Toutefois, cette recharge n'est que de courte durée et il faudra alors trouver un nouveau coffre de protection en un temps plus court que le précédent. Au final, les aventuriers passent donc la majorité de leur temps de tranquillité à traquer les apparitions de coffres et à se précipiter sur eux pour y collecter les précieux trésors.
 
La plupart des coffres contiennent des potions de lune, mais certains, plus rares, contiennent des potions de dissimulation, plus puissantes et capables de rendre un aventurier définitivement invisible aux yeux du dragon, sans avoir besoin de retrouver une potion de lune. D'autres contiennent une des pierres magiques tant recherchées. Mais attention, d'autres coffres contiennent des gaz mortels qui stoppent net la quête de l'aventurier.

La technologie actuelle permet facilement de détecter à distance les apparitions de coffres, mais elle ne permet pas de discerner leurs contenus. Les aventuriers qui ouvrent un coffre ne savent donc pas s’ils vont y trouver une potion de lune, une potion de dissimulation, un trésor ou du gaz mortel. 
 
Tout aventurier qui quitte sa ZRR avant que le TTL soit écoulé ne participe plus à la quête, même s'il est sous la protection d'une potion de dissimulation.

Dans sa volonté d'équiper au mieux les aventuriers, la Guilde des Trouvailles a lancé un grand projet d’application Web mobile destiné à permettre à ses membres de trouver plus facilement les apparitions de coffres pendant les quêtes. La première phase, qui a consisté à équiper tous les membres de la Guilde de terminaux mobiles géolocalisés, a déjà eu lieu. Il ne reste plus qu’à développer l’application…
 
La renommée que de telles découvertes rapporteraient à la Guilde des Trouvailles est tellement importante, que celle-ci a mandaté un cabinet de conseil offshore pour lancer un appel d’offres à plusieurs startups, dont vous faites partie. Votre mission, si vous l’acceptez, sera de développer d’une part une API permettant à la Guilde de mettre à disposition de ses membres les informations dont elle dispose, et d’autre part un client Web mobile permettant aux aventuriers de faire le meilleur usage de ces informations. Il est à noter que cette mission comporte, en plus de ces deux objectifs, une partie confidentielle qui vous sera dévoilée plus loin.

## API (TP3 & 4)

### Partie publique

Afin d’aider les membres de la Guilde des Trouvailles à mener efficacement les quêtes, vous développerez une API fournissant les informations suivantes :

- Limites de la ZRR
- Position de l’aventurier dans sa ZRR
- Points d'apparition des coffres au trésor dans sa ZRR
- Contenu d'un coffre dont l'aventurier s'est rapproché à moins de 5 mètres
- Rappel du TTL et du statut de l’aventurier : protégé temporairement ou définitivement par une potion, dévoré par le dragon ou tué par le gaz
- Montant des trésors récoltés par l’aventurier (et montant des 50% prélevés par la GT...)

### Partie confidentielle

Afin d’aider la Guilde des Trouvailles à optimiser les recherches, il vous est également demandé de développer une API qui ne sera pas accessible aux aventuriers, pour fournir aux Êtres Suprêmes de la Guilde la possibilité de :

- Fixer les limites des ZRR
- Surveiller l’état (position, temps de vie restant) et l’évolution (historique des récupérations de trésors) de l’aventurier de chaque ZRR
- Désenchanter un coffre au trésor dans une ZRR pour le rendre visible à son aventurier et lui faire croire qu’il est assez valeureux pour que des puissances anciennes lui révèlent l'existence d'un coffre magique qui a traversé les âges
- Permettre à l'Être Suprême de choisir le contenu du coffre en fonction de son humeur et de son opinion sur la valeur de l'aventurier : potion de lune, potion de dissimulation, gaz ou autre trésor (à vous de l'inventer)

## Client Web (TP 5 -> 8)

### Partie publique

Il vous est demandé de mettre à disposition des aventuriers de la Guilde un client aussi ergonomique et agréable à utiliser que possible, les informant avec précision de leur position, de celle des autres aventuriers de leur ZRR, ainsi que de celle des coffres au trésor (et si elle est connue, de leur composition). Ils pourront visualiser tout cela sur une carte montrant les limites de la ZRR, et surveiller en parallèle leur TTL. Pour pouvoir l’utiliser, ils devront s’identifier sur le serveur d’authentification centralisée de la Guilde (que vous avez réalisé aux TPs 1 et 2).

### Partie confidentielle

En plus des fonctionnalités de ce client, vous devrez aussi réaliser une interface de consultation et de modification des données réservée aux Guides Suprêmes de la Guilde des Trouvailles pour permettre d’utiliser les fonctionnalités de la partie réservée de l’API. Bien entendu, seuls ces utilisateurs y auront accès à travers une interface "back-office" séparée du client mobile.

<hr>

Crédit: Photo by <a href="https://unsplash.com/@jplenio?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Johannes Plenio</a> on <a href="https://unsplash.com/collections/CKvwqealkTc/enchanted-forest?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

# TP 6 - Framework côté client (suite)

## Objectifs

- Maîtriser la logique réactive des frameworks JS modernes
- Prendre en main un store centralisé pour un framework en JS : VueX
- Expérimenter les APIs de stockage de données en JS

## Pointeurs

- [Vue.js](https://vuejs.org/)
- [VueX](https://vuex.vuejs.org/)
  - [exemple simple](https://vuex.vuejs.org/guide/)
  - [principes](https://vuex.vuejs.org/guide/state.html)
  - [installation](https://vuex.vuejs.org/installation.html)
  - [code détaillé d'exemples vue2 et vuex3](https://github.com/vuejs/vuex/blob/dev/examples/)
- [vue cli](https://cli.vuejs.org/)
- [devtools](https://github.com/vuejs/vue-devtools)

## Application

Dans ce TP, vous poursuivrez la réalisation du client pour la partie publique du jeu. Cet énoncé part du principe que :

- La gestion des utilisateurs est disponible HTTPS dans Tomcat.<br>Si vous n'avez pas installé le certificat TLS dans Tomcat (et uniquement dans ce cas), vous pouvez y accéder via l'URL `https://proxy-tps-m1if13-2019.univ-lyon1.fr/XX/`.
- L'API du jeu est accessible en HTTPS sur le proxy nginx de votre VM ; l'API d'administration est déployée sur la route `/admin`, et l'API publique sur la route `/game`.
- L'interface "confidentielle" (d'administration) fonctionne également pour pouvoir configurer le jeu ; elle ne sera pas utilisée dans ce TP mais vous en aurez besoin pour tester.<br>Alternativement, vous pouvez utiliser Postman pour requêter votre API admin et configurer le jeu.
- Le client pour la partie publique du jeu est réalisé (avec un métier fonctionnel et des données mockées) et contient [vos premiers composants et votre routeur VueJS](../tp5/).

## 1. Mise en place du store VueX

Dans ce TP, vous allez modifier la façon dont Vue accède aux données du jeu, en appliquant le State Management pattern. Pour cela, VueX fournit un espace centralisé pour gérer l'état des composants de votre application, et ainsi faciliter leur partage.

**Rappel :** [Deux principes importants](https://v3.vuex.vuejs.org/fr/guide/) à retenir sur les stores Vuex :

> 1. Les stores Vuex sont réactifs. Quand les composants Vue y récupèrent l'état, ils se mettront à jour de façon réactive et efficace si l'état du store a changé.
>
> 2. Vous ne pouvez pas muter directement l'état du store. La seule façon de modifier l'état d'un store est d'acter (« commit ») explicitement des mutations. Cela assure que chaque état laisse un enregistrement traçable, et permet à des outils de nous aider à mieux appréhender nos applications.

<hr>

**Remarque :** VueX est en passe d'être remplacé par [Pinia](https://pinia.vuejs.org/) (voir [ici](https://vuex.vuejs.org/#what-is-vuex)).

> The official state management library for Vue has changed to [Pinia](https://pinia.vuejs.org/). Pinia has almost the exact same or enhanced API as Vuex 5, described in [Vuex 5 RFC](https://github.com/vuejs/rfcs/pull/271). You could simply consider Pinia as Vuex 5 with a different name.

Pour l'instant, VueX n'a pas été remplacé par Pinia dans tous les outils "autour" de Vue (en particulier vue ui), et donc dans ce TP, nous continuerons à utiliser VueX. Comme indiqué, son fonctionnement est le même et vous acquerrez donc les mêmes compétences avec l'un et avec l'autre.


### 1.1. Démarrage avec Vuex

Ajoutez le module VueX dans votre projet, soit avec vue cli (`vue add vuex`), soit avec l'interface Web de vue ui (`vue ui`).

### 1.2. Création du store :

Vous allez maintenant créer un `store` en suivant une [structure de projet similaire à celle ci-dessous](https://vuex.vuejs.org/guide/structure.html) :

```
├── index.html
├── main.js
├── App.vue
├── api/
│   └── ...               # abstraction pour faire des requêtes par API
├── components/
│   └── ...
└── store/
    ├── index.js          # là où l'on assemble nos modules et exportons le store
    ├── actions.js        # actions de base
    ├── mutations.js      # mutations de base
    └── modules/          # modules optionnels
```

L'état va contenir les propriétés suivantes :

- les limites de la ZRR (envoyées par le serveur),
- les informations du joueur :
  - identifiant et icone (définies par le client Vue ; ces informations seront persistées en plus en localStorage),
  - les coordonnées du joueur (pour ce TP on les simulera avec des clics de la souris, on utilisera le GPS dans les prochains TPs),
  - le dernier TTL initial du joueur (envoyé par le serveur),
- les informations sur les coffres :
  - positions (envoyées par le serveur),
  - états : visités ou non (déterminés par le client), et si visité, contenus (envoyés par le serveur).

Voici un exemple de code qui vous permettra d'initialiser votre store :

```js
import { createStore } from 'vuex'
import game from '../../api/game'
import mutations from './mutations'
import actions from './actions'
import getters from './getters'

const store = createStore({
  state () {
    return {
      zrr: [[x0,y0], [x1,y1]],
      playerPosition : [x, y],
      ...
    }
  },
  mutations,
  actions,
  getters,
})

export default store;
```

**Aide :** vous trouverez un exemple complet VueJS et VueX pour le TP précédent et celui ci dans ce [Shopping Cart (Vue2+Vuex3)](https://github.com/vuejs/vuex/tree/dev/examples/shopping-cart)

### 1.3 Création des mutations :

Les mutations sont les transformation atomiques effectuées sur l'état de votre application.
Pour chaque élément du store créez les mutations correspondantes. Elle suivent souvent les principes de C(R)UD: ajout, suppression, modification. Il s'agit de modification alors pas de nécessité d'avoir la lecture (pas de read), vue se chargera de diffuser les changements d'état aux composants.

On voudra (liste liée aux propriétés du store) :

- créer la ZRR
- mettre à jour l'identifiant et l'icone du joueur
- créer et mettre à jour la position du joueur
- créer et modifier le TTL donné par le serveur
- créer des coffres et stocker leurs positions
- mettre à jour les états des coffres (visité, type de contenu)

Commencez par définir l'ensemble des fonctions de _mutation_ dans votre store, comme indiqué [ici](https://vuex.vuejs.org/guide/mutations.html).

Vous pourrez ensuite les déclencher à l'aide de _commits_. Par exemple : `store.commit('defineZrr', [[x0,y0], [x1,y1]])`.

### 1.4 Création des actions :

Les _actions_ sont les fonctions qui seront appelées dans votre application pour effectuer les commits liés aux mutations. Ces actions seront déclenchées en fonction de l'arrivée des données à modifier.

Pour les actions liées à des changements côtés serveur, vous pouvez vous inspirer de l'exemple du [chat](https://github.com/vuejs/vuex/tree/dev/examples/chat) côté `api/` et `store/` vous pourrez voir comment mettre à jour l'état après une requête à un serveur.

**Aide :** pour tester, vous pouvez créer un bouton `mise à jour` pour envoyer une requête au serveur, et déclencher l'action dans le callback de la réponse.

Pour les actions liées à des changements côtés client, l'idée est à peu près la même : les actions seront appelées adns les fonctions récupérant les données à modifier.

Par exemple, vous allez créer une action qui va artificiellement modifier la position du joueur. Pour cela ajoutez un `EventListener` au clic sur le composant contenant la carte, et récupérez les coordonnées du point cliqué (voir Events dans la doc de Leaflet). Dans le callback, mettez à jour la position de l'utilisateur dans le store avec ces coordonnées.

Pour la liste des actions à réaliser, vous vous reporterez à la [section sur les fonctionnalités](#3-fonctionnalités-à-réaliser) ci-dessous.

### 1.5. Création des getters :

Si Vue diffuse bien les changements d'état, il peut être utile de fournir à l'application un état dérivé d'un autre état. Par exemple :

- le TTL courant à partir du dernier TTL envoyé par le serveur : si vous faites une requête toutes les 5 secondes, cela permettra de faire décroître le TTL dans l'interface entre 2 réponses du serveur.
- la distance entre un joueur et un coffre.

On utilise pour cela [des getters](https://vuex.vuejs.org/guide/getters.html).

Une fois les getters définis, ils sont accessibles depuis l'objet `store` : `store.getters.ttl` ("object-style"), ou `store.getters.getDistanceFrom(objX)` ("method-style").

**Remarque :** Ne créez des getters que si vous en ressentez le besoin. Ce n'est pas obligatoire.

### 2. Liens entre Store et Composants

Vous allez maintenant utiliser le data-flow réactif pour mettre à jour les composants.

En plus de l'utilisation d'actions pour déclencher les modifications d'états, vous allez [connecter l'état de vos composants au store](https://vuex.vuejs.org/guide/state.html#getting-vuex-state-into-vue-components). Pour cela :

- créez dans vos composants des _computed properties_ qui feront appel aux propriétés du store et seront déclenchées dynamiquement par VueX à chaque modification
- utilisez ces computed properties dans les templates de vos composants

Voir par exemple cette [todo list](https://github.com/vuejs/vuex/blob/dev/examples/todomvc/).

### 3. Fonctionnalités à réaliser

Utilisez l'API Web storage pour stocker :

- le token renvoyé par le serveur Spring
- les informations de base de l'utilisateur (identifiant, URL de l'image représentant l'icone de l'utilisateur sur la carte)

Requêtez le serveur Express toutes les 5 secondes, avec :

- les coordonnées du joueur dans la requête
- Récupération de la réponse avec :
  - le TTL du joueur
  - éventuellement, la position d'un nouveau coffre

Stockez dans le store :

  - la limite de la ZRR
  - les coordonnées du joueur
  - le TTL initial du joueur
  - la position des coffres

Sur l'interface, affichez :

- le TTL
- la distance (et éventuellement la direction) entre le joueur et le coffre non ouvert

Sur la carte, faites apparaître :

  - les limites de la ZRR (autour du Nautibus)
  - l'icone du joueur courant à la position cliquée
  - les coffres

Lorsque le TTL est expiré ou lorsque le joueur a rejoint un coffre (à moins de 2m), faites en sorte que l'affichage affiche un nouveau composant textuel par-dessus la carte, avec les informations correspondantes.
